-- Copyright (c) 2013 Robert Roland.
-- Released under the terms of the MIT license. See LICENSE.

import Control.Monad                (forM, liftM)
import Control.Monad.IO.Class       (liftIO)
import Control.Monad.Parallel as P  (mapM)
import Control.Monad.Trans.Resource (release)
import System.Directory (doesDirectoryExist, getDirectoryContents)
import System.Environment
import System.FilePath ((</>))

import Database.LevelDB

import ID3 (ID3Tag, readTag, header)
import ID3.Simple

data MusicTrack = MusicTrack { fileName :: String
                             , title :: Maybe String
                             , artist :: Maybe String
                             , album :: Maybe String
                             , year :: Maybe String
                             , comment :: Maybe String
                             , track :: Maybe String
                             , genre :: Maybe String
                             } deriving (Show)

main :: IO ()
main = do
  tags <- processTags
  
  P.mapM buildAndDisplayTag tags
  
  return ()
          
buildAndDisplayTag :: (String, IO (Maybe ID3Tag)) -> IO ()
buildAndDisplayTag (fileName, tag) = do
  unwrappedTag <- tag
      
  case unwrappedTag of
    Just tag -> do 
      let track = buildMusicTrack fileName tag
      putStrLn (show track)
    Nothing -> return ()
  
  return ()
    
processTags :: IO [(String, IO (Maybe ID3Tag))]
processTags = do
  args <- getArgs
  contents <- getRecursiveContents (head args)
  tags <- forM contents $ \fileName -> return (fileName, readTag fileName)
  
  return tags

buildMusicTrack :: FilePath -> ID3Tag -> MusicTrack
buildMusicTrack fileName tag =
  MusicTrack { fileName = fileName
             , title = getTitle tag
             , artist = getArtist tag
             , album = getAlbum tag
             , year = getYear tag
             , track = getTrack tag
             , comment = Nothing
             , genre = Nothing
             }

printTag :: Maybe ID3Tag -> IO ()
printTag (Just tag) = do
  let title = getTitle tag
  case title of
    Just value -> putStrLn value
    Nothing -> putStrLn "bad tag"

printTag Nothing = putStrLn "Not a tag"
        
getRecursiveContents :: FilePath -> IO [FilePath]
getRecursiveContents topdir = do
  names <- getDirectoryContents topdir
  let properNames = filter (`notElem` [".", ".."]) names
  paths <- forM properNames $ \name -> do
    let path = topdir </> name
    isDirectory <- doesDirectoryExist path
    if isDirectory
      then getRecursiveContents path
      else return [path]
  return (concat paths)
